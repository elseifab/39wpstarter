#!/bin/sh

echo ""
echo "*****************************************************"
echo "vagrant provision script enabled"
echo "*****************************************************"

if which ansible >/dev/null; then
	echo "ansible (assumed) already installed"
else
	echo "installing ansible and git to localhost"
	echo "approx 1-5 minutes..."
	sudo apt-get remove -y grub-pc
	sudo apt-get remove -y --purge virtualbox-ose-guest-x11
	sudo apt-get autoremove
	sudo apt-get remove -y --purge virtualbox-ose-guest-utils
	sudo apt-get install -y python-software-properties
	sudo add-apt-repository -y ppa:rquillo/ansible
	sudo apt-get update
	#sudo apt-get upgrade -y
	sudo apt-get install -y ansible git
fi



